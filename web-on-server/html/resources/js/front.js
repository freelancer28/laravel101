$(document).ready(function() {
    const urlAjax = 'https://www.huuhung.blog/ajax/callback';
    const _token_jax = $('meta[name="csrf-token"]').attr('content');

    const sendAjaxPromise = (url, type, data) => new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (result) {
                resolve(result);
            },
            error: function (error) {
                reject(error);
            }
        });
    });

    $('#login-form').on('submit', async function(e) {

        e.preventDefault();
        let $data = $(this).serialize();
        try {

            const result = await sendAjaxPromise( urlAjax, 'post', {
                "_token":  _token_jax,
                action: 'ajax_login',
                data: $data ,
            });
            let $result = JSON.parse( result ), return_mess =$(this).find('.messenger') ,
                response = $result.response , $messenger = '' , $redirect = '' ;

            return_mess.css('color' , 'blue');
            if( response.status ==  401 ) {
                return_mess.css('color' , 'red');
                $messenger = response.messenger;
            }else {
                $messenger = response.messenger.mess;
                $redirect = response.messenger.redirect;
                // window.location.href=$redirect;
            }

            return_mess.text($messenger);

        } catch(e) {

            console.log(e);
        }

    });
});
