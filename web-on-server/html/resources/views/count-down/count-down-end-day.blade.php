<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{asset('assets/images/vietnam.png')}}" type="image/x-icon">
        <title>Count down Vietnamese new year 2021</title>
        {{-- SEO --}}
        <meta property="og:locale" content="vi_VN">
        <meta property="og:type" content="article">
        <meta property="og:title" content="Count down giờ về">
        <meta property="og:description" content="Count down giờ về">
        <meta property="og:site_name" content="Hùng Nguyễn Blog ">
        <meta property="og:image" content="{{asset('assets/images/gio-ve.jpeg')}}">
        <meta property="og:image:width" content="1808">
        <meta property="og:image:height" content="2560">
        {{-- <script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"http://huuhung.blog/#website","url":"http://huuhung.blog/","name":"Gi\u1ea3i Ph\u00e1p Y Khoa","description":"","potentialAction":[{"@type":"SearchAction","target":"http://giaiphapykhoa.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"vi-VN"},{"@type":"ImageObject","@id":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/#primaryimage","inLanguage":"vi-VN","url":"http://giaiphapykhoa.com/wp-content/uploads/2020/12/sieuamvubiafull-scaled.jpg","width":1808,"height":2560},{"@type":"WebPage","@id":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/#webpage","url":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/","name":"TH\u1ef0C H\u00c0NH SI\u00caU \u00c2M V\u00da - Gi\u1ea3i Ph\u00e1p Y Khoa","isPartOf":{"@id":"http://giaiphapykhoa.com/#website"},"primaryImageOfPage":{"@id":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/#primaryimage"},"datePublished":"2020-12-10T08:27:14+00:00","dateModified":"2021-01-06T04:42:34+00:00","inLanguage":"vi-VN","potentialAction":[{"@type":"ReadAction","target":["http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/"]}]}]}</script> --}}
        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet"> --}}
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>
        <?php $rand = rand(1,23) ?>
        <style>

            body {
                background-color: #f2f1ed;
                background-repeat: no-repeat;
                width: 100%;
                height: 100vh;
                background-size: cover;
                background-attachment: fixed;
                background-position: center;
                background-image: url({{asset("assets/images/rand/rand-$rand.jpg")}})
            }
            .wrap {
                position: absolute;
                bottom: 0;
                top: 0;
                left: 0;
                right: 0;
                margin: auto;
                height: 310px;
            }
            a {
                text-decoration: none;
                color: #1a1a1a;
            }
            h1 {
                margin-bottom: 60px;
                text-align: center;
                font: 300 2.25em 'Lato';
                text-transform: uppercase;
                text-shadow: 2px 2px 11px #fff;
            }
            h1 strong {
                font-weight: 400;
                color: #ea4c4c;
                text-shadow: 2px 2px 11px #746f6f;
            }
            h2 {
                margin-bottom: 80px;
                text-align: center;
                font: 300 0.7em 'Lato';
                text-transform: uppercase;
            }
            h2 strong {
                font-weight: 400;
            }
            .countdown {
                width: 980px;
                margin: 0 auto;
                max-width: 100%;
                text-align: center;
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
            }
            .countdown .bloc-time {
                float: left;
                margin-right: 45px;
                text-align: center;
                margin: 15px;
            }
            /* .countdown{
                display: flex;
                flex-wrap: wrap;
                justify-content: center;
            }
            .countdown .bloc-time {
                margin: 15px;
            } */
            .countdown .bloc-time:last-child {
                margin-right: 0;
            }
            .countdown .count-title {
                display: block;
                margin-bottom: 15px;
                font: normal 0.94em 'Lato';
                color: #fff;
                text-transform: uppercase;
                text-shadow: 1px 1px 10px black;
            }
            .countdown .figure {
                position: relative;
                float: left;
                height: 110px;
                width: 100px;
                margin-right: 10px;
                background-color: #fff;
                border-radius: 8px;
                box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), inset 2px 4px 0 0 rgba(255, 255, 255, 0.08);
            }
            .countdown .figure:last-child {
                margin-right: 0;
            }
            .countdown .figure > span {
                position: absolute;
                left: 0;
                right: 0;
                margin: auto;
                font: normal 5.94em/107px 'Lato';
                font-weight: 700;
                color: #de4848;
            }
            .countdown .figure .top:after, .countdown .figure .bottom-back:after {
                content: "";
                position: absolute;
                z-index: -1;
                left: 0;
                bottom: 0;
                width: 100%;
                height: 100%;
                border-bottom: 1px solid rgba(0, 0, 0, .1);
            }
            .countdown .figure .top {
                z-index: 3;
                background-color: #f7f7f7;
                transform-origin: 50% 100%;
                -webkit-transform-origin: 50% 100%;
                border-top-right-radius: 10px;
                border-top-left-radius: 10px;
                transform: perspective(200px)
            }
            .countdown .figure .bottom {
                z-index: 1;
            }
            .countdown .figure .bottom:before {
                content: "";
                position: absolute;
                display: block;
                top: 0;
                left: 0;
                width: 100%;
                height: 50%;
                background-color: rgba(0, 0, 0, .02);
            }
            .countdown .figure .bottom-back {
                z-index: 2;
                top: 0;
                height: 50%;
                overflow: hidden;
                background-color: #f7f7f7;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
            }
            .countdown .figure .bottom-back span {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                margin: auto;
            }
            .countdown .figure .top, .countdown .figure .top-back {
                height: 50%;
                overflow: hidden;
                backface-visibility: hidden
            }
            .countdown .figure .top-back {
                z-index: 4;
                bottom: 0;
                background-color: #fff;
                -webkit-transform-origin: 50% 0;
                transform-origin: 50% 0;
                transform: perspective(200px) rotateX(180deg);
                border-bottom-left-radius: 10px;
                border-bottom-right-radius: 10px;
            }
            .countdown .figure .top-back span {
                position: absolute;
                top: -100%;
                left: 0;
                right: 0;
                margin: auto;
            }


        </style>

    </head>
    <body>


    <div class="wrap">

        <h1>Sắp đến <strong>GIỜ VỀ</strong> rồi anh em</h1>

        <div class="countdown">
        <div class="bloc-time hours" data-init-value="00">
            <span class="count-title">Hours</span>

            <div class="figure hours hours-1">
            <span class="top">0</span>
            <span class="top-back">
                <span>0</span>
            </span>
            <span class="bottom">0</span>
            <span class="bottom-back">
                <span>0</span>
            </span>
            </div>

            <div class="figure hours hours-2">
            <span class="top">0</span>
            <span class="top-back">
                <span>0</span>
            </span>
            <span class="bottom">0</span>
            <span class="bottom-back">
                <span>0</span>
            </span>
            </div>
        </div>

        <div class="bloc-time min" data-init-value="0">
            <span class="count-title">Minutes</span>

            <div class="figure min min-1">
            <span class="top">0</span>
            <span class="top-back">
                <span>0</span>
            </span>
            <span class="bottom">0</span>
            <span class="bottom-back">
                <span>0</span>
            </span>
            </div>

            <div class="figure min min-2">
            <span class="top">0</span>
            <span class="top-back">
                <span>0</span>
            </span>
            <span class="bottom">0</span>
            <span class="bottom-back">
                <span>0</span>
            </span>
            </div>
        </div>

        <div class="bloc-time sec" data-init-value="0">
            <span class="count-title">Seconds</span>

            <div class="figure sec sec-1">
            <span class="top">0</span>
            <span class="top-back">
                <span>0</span>
            </span>
            <span class="bottom">0</span>
            <span class="bottom-back">
                <span>0</span>
            </span>
            </div>

            <div class="figure sec sec-2">
            <span class="top">0</span>
            <span class="top-back">
                <span>0</span>
            </span>
            <span class="bottom">0</span>
            <span class="bottom-back">
                <span>0</span>
            </span>
            </div>
        </div>
        </div>
    </div>
    {{-- <button onclick="notifyMe()">Notify me!</button> --}}

    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>

    <script>
    // Create Countdown
    // setTimeout(function(){  window.location.reload( ) }, 60000);
    document.addEventListener("visibilitychange", function() {
        let time  = 1000 * 6 * 5  ;
        if( document.visibilityState !== 'visible' ) {
            console.log('out');
            var reset =  setTimeout(function(){

                window.location.reload();
                // console.log('run');

            }, time );

        }
        if( document.visibilityState === 'visible' ) {

            clearTimeout( reset ) ;

        }

    });

    var Countdown = {


        $el: $('.countdown'),


        countdown_interval: null,
        total_seconds     : 0,

        init: function() {
            var today = (new Date()).toLocaleDateString('en-US');

            const endtime = today + ' 18:00:00 GMT+0700';
            const total = Date.parse(endtime) - Date.parse(new Date());

            const seconds = Math.floor( (total/1000) % 60 );
            const minutes = Math.floor( (total/1000/60) % 60 );
            const hours = Math.floor( (total/(1000*60*60)) % 24 );
            const days = Math.floor( total/(1000*60*60*24) );


            this.$ = {

                hours  : this.$el.find('.bloc-time.hours .figure'),
                minutes: this.$el.find('.bloc-time.min .figure'),
                seconds: this.$el.find('.bloc-time.sec .figure')
            };

            this.values = {

                hours  : hours, //this.$.hours.parent().attr('data-init-value'),
                minutes: minutes , // this.$.minutes.parent().attr('data-init-value'),
                seconds: seconds, //this.$.seconds.parent().attr('data-init-value'),
            };

            this.total_seconds =  this.values.hours * 60 * 60 + (this.values.minutes * 60) + this.values.seconds;
            // console.log(this.total_seconds);

            this.count();
        },

        count: function() {

            var that    = this,

                $hour_1 = this.$.hours.eq(0),
                $hour_2 = this.$.hours.eq(1),
                $min_1  = this.$.minutes.eq(0),
                $min_2  = this.$.minutes.eq(1),
                $sec_1  = this.$.seconds.eq(0),
                $sec_2  = this.$.seconds.eq(1);

                this.countdown_interval = setInterval(function() {

                console.log( that.values.seconds , that.values.hours ,that.values.minutes );
                if (that.values.hours == 06  &&  that.values.minutes == 00 &&  that.values.seconds == 00) {
                    notifyMe( 'Nghỉ trưa' , "Đi ăn trưa thôi ae" );
                }
                if (that.values.hours == 00  &&  that.values.minutes == 05 &&  that.values.seconds == 00) {
                    notifyMe( 'Sắp 6h rồi' , "Còn 5p nữa là về" );
                }
                if (that.values.hours == 00  &&  that.values.minutes == 00 &&  that.values.seconds == 00) {
                    notifyMe( 'Đã 6h rồi' , "Về thôi Ae" );
                }


                if(that.total_seconds > 0) {

                    --that.values.seconds;

                    if(that.values.minutes >= 0 && that.values.seconds < 0) {

                        that.values.seconds = 59;
                        --that.values.minutes;
                    }

                    if(that.values.hours >= 0 && that.values.minutes < 0) {

                        that.values.minutes = 59;
                        --that.values.hours;
                    }
                    // Hours
                    that.checkHour(that.values.hours, $hour_1, $hour_2);

                    // Minutes
                    that.checkHour(that.values.minutes, $min_1, $min_2);

                    // Seconds
                    that.checkHour(that.values.seconds, $sec_1, $sec_2);

                    --that.total_seconds;
                }
                else {
                    clearInterval(that.countdown_interval);
                }
            }, 1000);
        },

        animateFigure: function($el, value) {

            var that         = this,
                $top         = $el.find('.top'),
                $bottom      = $el.find('.bottom'),
                $back_top    = $el.find('.top-back'),
                $back_bottom = $el.find('.bottom-back');


            $back_top.find('span').html(value);


            $back_bottom.find('span').html(value);

             // lat lat
            TweenMax.to($top, 0.8, {
                rotationX           : '-180deg',
                transformPerspective: 300,
                ease                : Quart.easeOut,
                onComplete          : function() {

                    $top.html(value);

                    $bottom.html(value);

                    TweenMax.set($top, { rotationX: 0 });
                }
            });

            TweenMax.to($back_top, 0.8, {
                rotationX           : 0,
                transformPerspective: 300,
                ease                : Quart.easeOut,
                clearProps          : 'all'
            });
        },

        checkHour: function(value, $el_1, $el_2) {

            var val_1       = value.toString().charAt(0),
                val_2       = value.toString().charAt(1),
                fig_1_value = $el_1.find('.top').html(),
                fig_2_value = $el_2.find('.top').html();

            if(value >= 10) {


                if(fig_1_value !== val_1) this.animateFigure($el_1, val_1);
                if(fig_2_value !== val_2) this.animateFigure($el_2, val_2);
            }
            else {


                if(fig_1_value !== '0') this.animateFigure($el_1, 0);
                if(fig_2_value !== val_1) this.animateFigure($el_2, val_1);
            }
        }
        };

        // chạy nè
        Countdown.init();
        // thong bao ne
        // request permission on page load
        document.addEventListener('DOMContentLoaded', function() {
            if (!Notification) {
                alert('Desktop notifications not available in your browser. Try Chromium.');
                return;
            }

            if (Notification.permission !== 'granted')
                Notification.requestPermission().then(function(permission) { console.log( permission ) });
            });


            function notifyMe( title , description ) {
                if (Notification.permission !== 'granted'){
                    Notification.requestPermission().then(function(permission) { console.log( permission ) });
                } else {
                var notification = new Notification( title , {
                    icon: 'https://huuhung.blog/assets/images/1f555.png',
                    body: description ,
                });
                notification.onclick = function() {
                    window.open('https://huuhung.blog/count-down-6-gio');
                };
            }
        }

    </script>
</html>
