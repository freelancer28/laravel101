<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{asset('assets/images/2020.png')}}" type="image/x-icon">
        <title>Party end year 2020 </title>
        {{-- SEO --}}
        <meta property="og:locale" content="vi_VN">
        <meta property="og:type" content="article">
        <meta property="og:title" content="Party end year 2020">
        <meta property="og:description" content="Party end year 2020 by Hùng Nguyễn ">
        <meta property="og:site_name" content="Hùng Nguyễn Blog ">
        <meta property="og:image" content="{{asset('assets/images/2020.png')}}">
        <meta property="og:image:width" content="1808">
        <meta property="og:image:height" content="2560">
        {{-- <script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"http://huuhung.blog/#website","url":"http://huuhung.blog/","name":"Gi\u1ea3i Ph\u00e1p Y Khoa","description":"","potentialAction":[{"@type":"SearchAction","target":"http://giaiphapykhoa.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"vi-VN"},{"@type":"ImageObject","@id":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/#primaryimage","inLanguage":"vi-VN","url":"http://giaiphapykhoa.com/wp-content/uploads/2020/12/sieuamvubiafull-scaled.jpg","width":1808,"height":2560},{"@type":"WebPage","@id":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/#webpage","url":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/","name":"TH\u1ef0C H\u00c0NH SI\u00caU \u00c2M V\u00da - Gi\u1ea3i Ph\u00e1p Y Khoa","isPartOf":{"@id":"http://giaiphapykhoa.com/#website"},"primaryImageOfPage":{"@id":"http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/#primaryimage"},"datePublished":"2020-12-10T08:27:14+00:00","dateModified":"2021-01-06T04:42:34+00:00","inLanguage":"vi-VN","potentialAction":[{"@type":"ReadAction","target":["http://giaiphapykhoa.com/san-pham/thuc-hanh-sieu-am-vu/"]}]}]}</script> --}}
        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet"> --}}
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>
            <?php $rand = rand(1,23) ?>
        <style>
            html{
                font-size: 10px;
            }

            @media only screen and (max-width : 1440px){
                html{
                    font-size: 8px;
                }
            }
            @media only screen and (max-width : 768px){
                html{
                    font-size: 6px;
                }
            }
            body{
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: center;
                    -ms-flex-pack: center;
                        justify-content: center;
                -webkit-box-align: center;
                    -ms-flex-align: center;
                        align-items: center;
                text-align: center;
                font-family: 'Montserrat', sans-serif;
                color: #fff;
                font-size: 2.2rem;
                line-height: 1.4;
                min-height: 100vh;
                position: relative;
                text-shadow: 4px 4px 14px rgba(0, 0, 0, 0.1);
                background-color: #f5ae85;
                background-repeat: no-repeat;
                background-size: cover;
                background-position: center bottom;
                background-image: url({{asset("assets/images/bg.jpg")}});
            }
            a{
                text-decoration: none;
                color: inherit;
            }
            *, *::before, *::after{
                margin: 0;
                padding: 0;
                -webkit-box-sizing: border-box;
                        box-sizing: border-box;
            }
            .countdown{
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -ms-flex-wrap: wrap;
                    flex-wrap: wrap;
                -webkit-box-pack: center;
                    -ms-flex-pack: center;
                        justify-content: center;
            }
            .countdown .bloc-time {
                text-align: center;
                margin: 15px;
            }
            .countdown .bloc-time:last-child {
                margin-right: 0;
            }
            .countdown .count-title {
                display: block;
                margin-bottom: 10px;
                color: #fff;
                text-transform: uppercase;
            }
            .countdown .figure {
                position: relative;
                float: left;
                height: 110px;
                width: 90px;
                margin-right: 10px;
                background-color: #fff;
                border-radius: 8px;
                -webkit-box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), inset 2px 4px 0 0 rgba(255, 255, 255, 0.08);
                        box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), inset 2px 4px 0 0 rgba(255, 255, 255, 0.08);
                text-shadow: none;
            }

            @media only screen and (max-width : 768px){
                .countdown .bloc-time {
                    margin: 10px;
                }
                .countdown .figure{
                    width: 70px;
                    height: 90px;
                }
            }
            @media only screen and (max-width : 375px){
                .countdown .figure{
                    width: 55px;
                    height: 80px;
                }
            }
            .countdown .figure:last-child {
                margin-right: 0;
            }
            .countdown .figure > span {
                position: absolute;
                left: 0;
                right: 0;
                margin: auto;
                font-size: 80px;
                font-weight: bold;
                color: #de4848;
            }
            @media only screen and (max-width : 768px){
                .countdown .figure > span {
                    font-size: 60px;
                    line-height: 1.5;
                }
            }
            @media only screen and (max-width : 375px){
                .countdown .figure > span {
                    font-size: 54px;
                    line-height: 1.5;
                }
            }
            .countdown .figure .top:after, .countdown .figure .bottom-back:after {
                content: "";
                position: absolute;
                z-index: -1;
                left: 0;
                bottom: 0;
                width: 100%;
                height: 100%;
                border-bottom: 1px solid rgba(0, 0, 0, .1);
            }
            .countdown .figure .top {
                z-index: 3;
                background-color: #f7f7f7;
                transform-origin: 50% 100%;
                -webkit-transform-origin: 50% 100%;
                border-top-right-radius: 10px;
                border-top-left-radius: 10px;
                -webkit-transform: perspective(200px);
                        transform: perspective(200px)
            }
            .countdown .figure .bottom {
                z-index: 1;
            }
            .countdown .figure .bottom:before {
                content: "";
                position: absolute;
                display: block;
                top: 0;
                left: 0;
                width: 100%;
                height: 50%;
                background-color: rgba(0, 0, 0, .02);
            }
            .countdown .figure .bottom-back {
                z-index: 2;
                top: 0;
                height: 50%;
                overflow: hidden;
                background-color: #f7f7f7;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
            }
            .countdown .figure .bottom-back span {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                margin: auto;
            }
            .countdown .figure .top, .countdown .figure .top-back {
                height: 50%;
                overflow: hidden;
                -webkit-backface-visibility: hidden;
                        backface-visibility: hidden
            }
            .countdown .figure .top-back {
                z-index: 4;
                bottom: 0;
                background-color: #fff;
                -webkit-transform-origin: 50% 0;
                transform-origin: 50% 0;
                -webkit-transform: perspective(200px) rotateX(180deg);
                        transform: perspective(200px) rotateX(180deg);
                border-bottom-left-radius: 10px;
                border-bottom-right-radius: 10px;
            }
            .countdown .figure .top-back span {
                position: absolute;
                top: -100%;
                left: 0;
                right: 0;
                margin: auto;
            }
            .party{
                width: 100%;
                max-width: 1200px;
                padding: 50px 15px;
                position: relative;
                z-index: 2;
            }
            .party-title-1{
                font-size: 3.6rem;
                display: block;
                margin: 0 0 1.5rem;
            }
            .party-title-2{
                font-size: 8rem;
                font-weight: 900;
                display: block;
                letter-spacing: 0.1em;
            }
            .party-countdown{
                margin: 6rem 0;
                overflow: hidden;
            }
            .party-manager, .party-welcome, .party-desc{
                font-weight: 500
            }
            .party-manager{
                font-size: 3rem;
                font-weight: bold;
            }
            .party-welcome{
                margin: 1rem 0;
            }
            @media only screen and (max-width : 500px){
                .party-title-2{
                    font-size: 7rem;
                }
            }
            #canvas{
                cursor: crosshair;
	            display: block;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 1;
            }
            #audio{
                position: fixed;
                opacity: 0;
                visibility: hidden;
                pointer-events: none;
            }
        </style>
    </head>
    <body>
        <audio id="audio" loop>
            <source src="{{asset('assets/mp3/HappyNewYear-ABBA.mp3')}}">
        </audio>
        <canvas id="canvas">Canvas is not supported in your browser.</canvas>
        <div class="party">
            <h1 class="party-title">
                <span class="party-title-1">Join us for</span>
                <span class="party-title-2">YEAR END PARTY</span>
            </h1>
            <div class="party-countdown">
                <div class="countdown">
                    <div class="bloc-time days" data-init-value="24">
                        <span class="count-title">Days</span>

                        <div class="figure days days-1">
                            <span class="top">2</span>
                            <span class="top-back">
                                <span>2</span>
                            </span>
                            <span class="bottom">2</span>
                            <span class="bottom-back">
                                <span>2</span>
                            </span>
                        </div>

                        <div class="figure days days-2">
                        <span class="top">4</span>
                        <span class="top-back">
                            <span>4</span>
                        </span>
                        <span class="bottom">4</span>
                        <span class="bottom-back">
                            <span>4</span>
                        </span>
                        </div>
                    </div>
                    <div class="bloc-time hours" data-init-value="24">
                        <span class="count-title">Hours</span>

                        <div class="figure hours hours-1">
                        <span class="top">2</span>
                        <span class="top-back">
                            <span>2</span>
                        </span>
                        <span class="bottom">2</span>
                        <span class="bottom-back">
                            <span>2</span>
                        </span>
                        </div>

                        <div class="figure hours hours-2">
                        <span class="top">4</span>
                        <span class="top-back">
                            <span>4</span>
                        </span>
                        <span class="bottom">4</span>
                        <span class="bottom-back">
                            <span>4</span>
                        </span>
                        </div>
                    </div>
                    <div class="bloc-time min" data-init-value="0">
                        <span class="count-title">Minutes</span>

                        <div class="figure min min-1">
                        <span class="top">0</span>
                        <span class="top-back">
                            <span>0</span>
                        </span>
                        <span class="bottom">0</span>
                        <span class="bottom-back">
                            <span>0</span>
                        </span>
                        </div>

                        <div class="figure min min-2">
                        <span class="top">0</span>
                        <span class="top-back">
                            <span>0</span>
                        </span>
                        <span class="bottom">0</span>
                        <span class="bottom-back">
                            <span>0</span>
                        </span>
                        </div>
                    </div>
                    <div class="bloc-time sec" data-init-value="0">
                        <span class="count-title">Seconds</span>

                        <div class="figure sec sec-1">
                        <span class="top">0</span>
                        <span class="top-back">
                            <span>0</span>
                        </span>
                        <span class="bottom">0</span>
                        <span class="bottom-back">
                            <span>0</span>
                        </span>
                        </div>

                        <div class="figure sec sec-2">
                        <span class="top">0</span>
                        <span class="top-back">
                            <span>0</span>
                        </span>
                        <span class="bottom">0</span>
                        <span class="bottom-back">
                            <span>0</span>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <p class="party-manager">HỮU HÙNG - ĐỨC MẠNH - NHỰT TÂN</p>
            <p class="party-welcome">WELCOME</h2>
            <p class="party-desc">18:00 AM, 4 Feb 2021 - 179 CAO THẮNG PHƯỜNG 12, QUẬN 10, TP.HCM</p>
        </div>


    </body>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script>
            window.addEventListener('load', () => {
                audio.play();
            });

                // when animating on canvas, it is best to use requestAnimationFrame instead of setTimeout or setInterval
            // not supported in all browsers though and sometimes needs a prefix, so we need a shim
            window.requestAnimFrame = ( function() {
                return window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            function( callback ) {
                                window.setTimeout( callback, 1000 / 60 );
                            };
            })();

            // now we will setup our basic variables for the demo
            var canvas = document.getElementById( 'canvas' ),
                    ctx = canvas.getContext( '2d' ),
                    // full screen dimensions
                    cw = window.innerWidth,
                    ch = window.innerHeight,
                    // firework collection
                    fireworks = [],
                    // particle collection
                    particles = [],
                    // starting hue
                    hue = 120,
                    // when launching fireworks with a click, too many get launched at once without a limiter, one launch per 5 loop ticks
                    limiterTotal = 5,
                    limiterTick = 0,
                    // this will time the auto launches of fireworks, one launch per 80 loop ticks
                    timerTotal = 80,
                    timerTick = 0,
                    mousedown = false,
                    // mouse x coordinate,
                    mx,
                    // mouse y coordinate
                    my;

            // set canvas dimensions
            canvas.width = cw;
            canvas.height = ch;

            // now we are going to setup our function placeholders for the entire demo

            // get a random number within a range
            function random( min, max ) {
                return Math.random() * ( max - min ) + min;
            }

            // calculate the distance between two points
            function calculateDistance( p1x, p1y, p2x, p2y ) {
                var xDistance = p1x - p2x,
                        yDistance = p1y - p2y;
                return Math.sqrt( Math.pow( xDistance, 2 ) + Math.pow( yDistance, 2 ) );
            }

            // create firework
            function Firework( sx, sy, tx, ty ) {
                // actual coordinates
                this.x = sx;
                this.y = sy;
                // starting coordinates
                this.sx = sx;
                this.sy = sy;
                // target coordinates
                this.tx = tx;
                this.ty = ty;
                // distance from starting point to target
                this.distanceToTarget = calculateDistance( sx, sy, tx, ty );
                this.distanceTraveled = 0;
                // track the past coordinates of each firework to create a trail effect, increase the coordinate count to create more prominent trails
                this.coordinates = [];
                this.coordinateCount = 3;
                // populate initial coordinate collection with the current coordinates
                while( this.coordinateCount-- ) {
                    this.coordinates.push( [ this.x, this.y ] );
                }
                this.angle = Math.atan2( ty - sy, tx - sx );
                this.speed = 2;
                this.acceleration = 1.05;
                this.brightness = random( 50, 70 );
                // circle target indicator radius
                this.targetRadius = 1;
            }

            // update firework
            Firework.prototype.update = function( index ) {
                // remove last item in coordinates array
                this.coordinates.pop();
                // add current coordinates to the start of the array
                this.coordinates.unshift( [ this.x, this.y ] );

                // cycle the circle target indicator radius
                if( this.targetRadius < 8 ) {
                    this.targetRadius += 0.3;
                } else {
                    this.targetRadius = 1;
                }

                // speed up the firework
                this.speed *= this.acceleration;

                // get the current velocities based on angle and speed
                var vx = Math.cos( this.angle ) * this.speed,
                        vy = Math.sin( this.angle ) * this.speed;
                // how far will the firework have traveled with velocities applied?
                this.distanceTraveled = calculateDistance( this.sx, this.sy, this.x + vx, this.y + vy );

                // if the distance traveled, including velocities, is greater than the initial distance to the target, then the target has been reached
                if( this.distanceTraveled >= this.distanceToTarget ) {
                    createParticles( this.tx, this.ty );
                    // remove the firework, use the index passed into the update function to determine which to remove
                    fireworks.splice( index, 1 );
                } else {
                    // target not reached, keep traveling
                    this.x += vx;
                    this.y += vy;
                }
            }

            // draw firework
            Firework.prototype.draw = function() {
                ctx.beginPath();
                // move to the last tracked coordinate in the set, then draw a line to the current x and y
                ctx.moveTo( this.coordinates[ this.coordinates.length - 1][ 0 ], this.coordinates[ this.coordinates.length - 1][ 1 ] );
                ctx.lineTo( this.x, this.y );
                ctx.strokeStyle = 'hsl(' + hue + ', 100%, ' + this.brightness + '%)';
                ctx.stroke();

                ctx.beginPath();
                // draw the target for this firework with a pulsing circle
                ctx.arc( this.tx, this.ty, this.targetRadius, 0, Math.PI * 2 );
                ctx.stroke();
            }

            // create particle
            function Particle( x, y ) {
                this.x = x;
                this.y = y;
                // track the past coordinates of each particle to create a trail effect, increase the coordinate count to create more prominent trails
                this.coordinates = [];
                this.coordinateCount = 5;
                while( this.coordinateCount-- ) {
                    this.coordinates.push( [ this.x, this.y ] );
                }
                // set a random angle in all possible directions, in radians
                this.angle = random( 0, Math.PI * 2 );
                this.speed = random( 1, 10 );
                // friction will slow the particle down
                this.friction = 0.95;
                // gravity will be applied and pull the particle down
                this.gravity = 1;
                // set the hue to a random number +-50 of the overall hue variable
                this.hue = random( hue - 50, hue + 50 );
                this.brightness = random( 50, 80 );
                this.alpha = 1;
                // set how fast the particle fades out
                this.decay = random( 0.015, 0.03 );
            }

            // update particle
            Particle.prototype.update = function( index ) {
                // remove last item in coordinates array
                this.coordinates.pop();
                // add current coordinates to the start of the array
                this.coordinates.unshift( [ this.x, this.y ] );
                // slow down the particle
                this.speed *= this.friction;
                // apply velocity
                this.x += Math.cos( this.angle ) * this.speed;
                this.y += Math.sin( this.angle ) * this.speed + this.gravity;
                // fade out the particle
                this.alpha -= this.decay;

                // remove the particle once the alpha is low enough, based on the passed in index
                if( this.alpha <= this.decay ) {
                    particles.splice( index, 1 );
                }
            }

            // draw particle
            Particle.prototype.draw = function() {
                ctx. beginPath();
                // move to the last tracked coordinates in the set, then draw a line to the current x and y
                ctx.moveTo( this.coordinates[ this.coordinates.length - 1 ][ 0 ], this.coordinates[ this.coordinates.length - 1 ][ 1 ] );
                ctx.lineTo( this.x, this.y );
                ctx.strokeStyle = 'hsla(' + this.hue + ', 100%, ' + this.brightness + '%, ' + this.alpha + ')';
                ctx.stroke();
            }

            // create particle group/explosion
            function createParticles( x, y ) {
                // increase the particle count for a bigger explosion, beware of the canvas performance hit with the increased particles though
                var particleCount = 30;
                while( particleCount-- ) {
                    particles.push( new Particle( x, y ) );
                }
            }

            // main demo loop
            function loop() {
                // this function will run endlessly with requestAnimationFrame
                requestAnimFrame( loop );

                // increase the hue to get different colored fireworks over time
                //hue += 0.5;

            // create random color
            hue= random(0, 360 );

                // normally, clearRect() would be used to clear the canvas
                // we want to create a trailing effect though
                // setting the composite operation to destination-out will allow us to clear the canvas at a specific opacity, rather than wiping it entirely
                ctx.globalCompositeOperation = 'destination-out';
                // decrease the alpha property to create more prominent trails
                ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
                ctx.fillRect( 0, 0, cw, ch );
                // change the composite operation back to our main mode
                // lighter creates bright highlight points as the fireworks and particles overlap each other
                ctx.globalCompositeOperation = 'lighter';

                // loop over each firework, draw it, update it
                var i = fireworks.length;
                while( i-- ) {
                    fireworks[ i ].draw();
                    fireworks[ i ].update( i );
                }

                // loop over each particle, draw it, update it
                var i = particles.length;
                while( i-- ) {
                    particles[ i ].draw();
                    particles[ i ].update( i );
                }

                // launch fireworks automatically to random coordinates, when the mouse isn't down
                if( timerTick >= timerTotal ) {
                    if( !mousedown ) {
                        // start the firework at the bottom middle of the screen, then set the random target coordinates, the random y coordinates will be set within the range of the top half of the screen
                        fireworks.push( new Firework( cw / 2, ch, random( 0, cw ), random( 0, ch / 2 ) ) );
                        timerTick = 0;
                    }
                } else {
                    timerTick++;
                }

                // limit the rate at which fireworks get launched when mouse is down
                if( limiterTick >= limiterTotal ) {
                    if( mousedown ) {
                        // start the firework at the bottom middle of the screen, then set the current mouse coordinates as the target
                        fireworks.push( new Firework( cw / 2, ch, mx, my ) );
                        limiterTick = 0;
                    }
                } else {
                    limiterTick++;
                }
            }

            // mouse event bindings
            // update the mouse coordinates on mousemove
            canvas.addEventListener( 'mousemove', function( e ) {
                mx = e.pageX - canvas.offsetLeft;
                my = e.pageY - canvas.offsetTop;
            });

            // toggle mousedown state and prevent canvas from being selected
            canvas.addEventListener( 'mousedown', function( e ) {
                e.preventDefault();
                mousedown = true;
            });

            canvas.addEventListener( 'mouseup', function( e ) {
                e.preventDefault();
                mousedown = false;
            });

            // once the window loads, we are ready for some fireworks!
            window.onload = loop;


    </script>
    <script>
    // Create Countdown
    // document.addEventListener("visibilitychange", function() {

    //     let time  = 1000 * 6 * 5 ;
    //     if( document.visibilityState !== 'visible' ) {

    //         var reset =  setTimeout(function(){

    //             window.location.reload();

    //         }, time );

    //     }
    //     if( document.visibilityState === 'visible' ) {
    //         clearTimeout( reset ) ;
    //     }

    // });

    // console.log(new Date() );
    var Countdown = {


        $el: $('.countdown'),

        countdown_interval: null,
        total_seconds     : 0,

        init: function() {
            const endtime = '2021-02-04 18:00:00 GMT+0700 (Indochina Time)';
            const total = Date.parse(endtime) - Date.parse(new Date().toLocaleString("en-US", {timeZone: "Asia/Ho_Chi_Minh"}));
            const seconds = Math.floor( (total/1000) % 60 );
            const minutes = Math.floor( (total/1000/60) % 60 );
            const hours = Math.floor( (total/(1000*60*60)) % 24 );
            const days = Math.floor( total/(1000*60*60*24) );


            this.$ = {
                days   : this.$el.find('.bloc-time.days .figure'),
                hours  : this.$el.find('.bloc-time.hours .figure'),
                minutes: this.$el.find('.bloc-time.min .figure'),
                seconds: this.$el.find('.bloc-time.sec .figure')
            };


            this.values = {
                days   : days,
                hours  : hours, //this.$.hours.parent().attr('data-init-value'),
                minutes: minutes , // this.$.minutes.parent().attr('data-init-value'),
                seconds: seconds, //this.$.seconds.parent().attr('data-init-value'),
            };
            // console.log(this.values)

            this.total_seconds = this.values.days * 24 * 60 * 60  + this.values.hours * 60 * 60  + this.values.minutes * 60  + this.values.seconds;

            this.count();
        },

        count: function() {

            var that    = this,
                $day_1  = this.$.days.eq(0),
                $day_2  = this.$.days.eq(1),
                $hour_1 = this.$.hours.eq(0),
                $hour_2 = this.$.hours.eq(1),
                $min_1  = this.$.minutes.eq(0),
                $min_2  = this.$.minutes.eq(1),
                $sec_1  = this.$.seconds.eq(0),
                $sec_2  = this.$.seconds.eq(1);

                this.countdown_interval = setInterval(function() {
                    // console.log(that.total_seconds);
                if(that.total_seconds > 0) {
                    // console.log(that.values.hours, that.values.minutes , that.values.seconds);
                    --that.values.seconds;

                    if(that.values.minutes >= 0 && that.values.seconds < 0) {

                        that.values.seconds = 59;
                        --that.values.minutes;
                    }

                    if(that.values.hours >= 0 && that.values.minutes < 0) {

                        that.values.minutes = 59;
                        --that.values.hours;
                    }

                    if(that.values.days >= 0 && that.values.hours < 0) {

                        that.values.minutes = 24;
                        --that.values.days;

                    }


                    // days
                    that.checkHour(that.values.days, $day_1, $day_2);

                    // Hours
                    that.checkHour(that.values.hours, $hour_1, $hour_2);

                    // Minutes
                    that.checkHour(that.values.minutes, $min_1, $min_2);

                    // Seconds
                    that.checkHour(that.values.seconds, $sec_1, $sec_2);

                    --that.total_seconds;

                } else {
                    clearInterval(that.countdown_interval);
                }

            }, 1000);
        },

        animateFigure: function($el, value) {

            var that         = this,
                $top         = $el.find('.top'),
                $bottom      = $el.find('.bottom'),
                $back_top    = $el.find('.top-back'),
                $back_bottom = $el.find('.bottom-back');


            $back_top.find('span').html(value);


            $back_bottom.find('span').html(value);

             // lat lat
            TweenMax.to($top, 0.8, {
                rotationX           : '-180deg',
                transformPerspective: 300,
                ease                : Quart.easeOut,
                onComplete          : function() {

                    $top.html(value);

                    $bottom.html(value);

                    TweenMax.set($top, { rotationX: 0 });
                }
            });

            TweenMax.to($back_top, 0.8, {
                rotationX           : 0,
                transformPerspective: 300,
                ease                : Quart.easeOut,
                clearProps          : 'all'
            });
        },

        checkHour: function(value, $el_1, $el_2) {

            var val_1       = value.toString().charAt(0),
                val_2       = value.toString().charAt(1),
                fig_1_value = $el_1.find('.top').html(),
                fig_2_value = $el_2.find('.top').html();

            if(value >= 10) {


                if(fig_1_value !== val_1) this.animateFigure($el_1, val_1);
                if(fig_2_value !== val_2) this.animateFigure($el_2, val_2);
            }
            else {


                if(fig_1_value !== '0') this.animateFigure($el_1, 0);
                if(fig_2_value !== val_1) this.animateFigure($el_2, val_1);
            }
        }
        };

        // chạy nè
        Countdown.init();
        // nhắc ae đem tiền
        window.addEventListener("beforeunload", function (e) {
            var confirmationMessage = "\o/";

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage;                            //Webkit, Safari, Chrome
        });
    </script>
</html>
