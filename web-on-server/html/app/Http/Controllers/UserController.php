<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class UserController extends Controller
{
    /**
     * Login user and create token
     * Doc https://www.toptal.com/laravel/passport-tutorial-auth-user-access
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return view( 'admin.login' , $this->error_messenger( 'Unauthorized' ));

            // return response()->json([
            //     'message' => 'Unauthorized'
            // ], 401);

        $user = $request->user();
        var_dump($user);exit;
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return view('admin.dashboard' , $this->success_messenger( 'Login Success' ));

        // return response()->json([
        //     'access_token' => $tokenResult->accessToken,
        //     'token_type' => 'Bearer',
        //     'expires_at' => Carbon::parse(
        //         $tokenResult->token->expires_at
        //     )->toDateTimeString()
        // ]);
    }
    public function ajax_login ($data_user) {
        $user_email = $data_user['email'];
        $user_password = $data_user['password'];
        $credentials = [
            'email' => $user_email ,
            'password' =>  $user_password
        ];
        if (Auth::check()) {
            echo json_encode($this->error_messenger( 'Đã đăng nhập rồi')) ;
            return;
        }
        if(!Auth::attempt($credentials)){
            echo json_encode($this->error_messenger( 'Sai thông tin đăng nhập')) ;
        }else {
            echo json_encode($this->success_messenger( [ 'mess' => 'Đúng thông tin đăng nhập' , 'redirect' => route('view-dashboard') ])) ;
        }
    }
    public function logout ( ) {
        return Auth::logout();
    }
}
