<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * function return response messenger (success)
    */
    public function success_messenger( $messenger ) {
        return ['response' => ['messenger' =>  $messenger , 'status' => 200 ]] ;
    }

    /**
     * function return response messenger (success)
    */
    public function error_messenger( $messenger ) {
        return ['response' => ['messenger' =>  $messenger , 'status' => 401 ]] ;
    }
}
