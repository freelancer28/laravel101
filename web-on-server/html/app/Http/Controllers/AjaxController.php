<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

class AjaxController extends Controller
{
    public function login ( ) {

    }

    public function getDownload( $name ) {

        $file = public_path(). "/images/file-in/".$name.".docx";

        $headers = array(
            'Content-Type: application/msword',
        );

        $result = response()->download($file, $name.'.doc', $headers);

        return  $result;
    }

    public function callback ( Request $request) {

        $data_post = $request->all( );
        if (isset($data_post['action']) and isset( $data_post['data']) ) {

            $action = $data_post['action'];
            $data = $data_post['data'];
            $this->$action($data);
        }else {
            return $this->error_messenger( 'Action none' );
        }
    }

    public function ajax_login($data) {

        $data_login = [];
        parse_str($data , $data_login);

        $UserClass = new UserController();

        $result = $UserClass->ajax_login($data_login ) ;
    }
}
