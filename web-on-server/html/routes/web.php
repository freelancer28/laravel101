<?php

use App\Http\Controllers\AjaxController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Route group admin
| Matches The "/backend-admin/*" URL
|--------------------------------------------------------------------------
*/
Route::prefix('backend-admin')->group( function () {

    Route::get('/', function () { #  làm lại redirect sau khi done login
        return view('admin.login');
    })->name('login-admin');

    Route::get('/dashboard', ['middleware' => ['auth' , 'web']] , function () {
        return view('admin.dashboard');
    })->name('view-dashboard');

    // Route::get('/login', function () {
    //     return view('admin.login');
    // })->name('login-admin');

});
/*
|--------------------------------------------------------------------------
| Route group ajax
| Matches The "/ajax/*" URL
|--------------------------------------------------------------------------
*/
Route::prefix('ajax')->group( function() {

    Route::post('/callback', [AjaxController::class, 'callback' ] );

});
/*
|--------------------------------------------------------------------------
| Route countdown
| Matches The "/count-down-viet-nam-new-year-2021/*" URL
|--------------------------------------------------------------------------
*/
Route::get('/count-down-vietnamese-new-year-2022', function () {
    return view('count-down.count-down');
})->name('count-down-new-year');

Route::get('/count-down-6-gio', function () {
    return view('count-down.count-down-end-day');
});
Route::get('/home', function () {
    return view('count-down.count-down-end-day');
});

// Route::get('/download-file/{name}' , [AjaxController::class, 'getDownload'] );
/*
|--------------------------------------------------------------------------
| Route party
| Matches The "/party-end-year/*" URL
|--------------------------------------------------------------------------
*/
Route::get('/party-end-year-2020', function () {
    return view('count-down.party-end-year');
});
